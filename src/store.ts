import { writable, readable, type Writable } from 'svelte/store';

type PaperType = "published" | "generated";
type Answer = "correct" | "incorrect" | "none";

function getShuffledArr(arr) {
    const newArr = arr.slice()
    for (let i = newArr.length - 1; i > 0; i--) {
        const rand = Math.floor(Math.random() * (i + 1));
        [newArr[i], newArr[rand]] = [newArr[rand], newArr[i]];
    }
    return newArr
};

const questionList: { is: PaperType, paperName: String, realDoi?: String }[] = [
    {
        "is": "published",
        "paperName": "TenniVis: Visualization for Tennis Match Analysis",
        "realDoi": "10.1109/TVCG.2014.2346445"
    },
    {
        "is": "generated",
        "paperName": "Abandoned Methods for Visual Analysis of Metabolites"
    },
    {
        "is": "generated",
        "paperName": "Spurious Exploratory Analysis of Inputs"
    },
    {
        "is": "published",
        "paperName": "Interactive Visual Profiling of Musicians",
        "realDoi": "10.1109/TVCG.2015.2467620"
    },
    {
        "is": "generated",
        "paperName": "HyDraw: Better Visual Guidance for Hydrolysis"
    },
    {
        "is": "published",
        "paperName": "Sketchy Rendering for Information Visualization",
        "realDoi": "10.1109/TVCG.2012.262"
    },
    {
        "is": "published",
        "paperName": "SkyLens: Visual Analysis of Skyline on Multi-dimensional Data",
        "realDoi": "10.1109/TVCG.2017.2744738"
    },
    {
        "is": "published",
        "paperName": "Glyph-Based Video Visualization for Semen Analysis",
        "realDoi": "10.1109/TVCG.2013.265"
    },
    {
        "is": "generated",
        "paperName": "Judicious Exploration Visual Guidance: State of the Art"
    },
    {
        "is": "generated",
        "paperName": "GoggLens: Fast Visual Synthesis using Goggles"
    }
];

export const ended = writable(false);

export const questions = readable<{ is: PaperType, paperName: String, realDoi?: String }[]>(questionList);
export const answers: Writable<Array<{ answered: boolean, answer: Answer }>> = writable(questionList.map(q => ({
    answered: false,
    answer: "none"
})));